/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciabancariafx;

/**
 *
 * @author Mateus
 */
public class Conta {
    private Pessoa titular;
    private int numero;
    private int digito;
    private float saldo;
    

    public Pessoa getTitular() {
        return titular;
    }

    public void setTitular(Pessoa titular) {
        this.titular = titular;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getDigito() {
        return digito;
    }

    public void setDigito(int digito) {
        this.digito = digito;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
    public boolean debita(float valor)
    {
        if(valor<this.saldo)
        {
            saldo-=valor; 
            return true;
        }  
        else
        {
           return false;
        }
    }
    public void credita(float valor)
    {
        saldo+=valor;
    }
}
