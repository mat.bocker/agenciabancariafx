/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciabancariafx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Mateus
 */
public class TelaAgenciaController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button BotaoP;
    @FXML
    private Button BotaoC;
    @FXML
    private TextField nome;
    @FXML
    private TextField endereco;
    @FXML
    private TextField cidade;
    @FXML
    private TextField bairro;
    @FXML
    private TextField estado;
    @FXML
    private TextField fone;
    @FXML
    private TextField email;
    @FXML
    private TextField cpf;
    @FXML
    private TextField rg;
    @FXML
    private TextField titular;
    @FXML
    private TextField numero;
    @FXML
    private TextField digito;
    @FXML
    private TextField saldo;
    @FXML
    private Button credit;
    @FXML
    private Label respcre;
    @FXML
    private Button debit;
    @FXML
    private Label respdeb;
    
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    Pessoa joao = new Pessoa();
    Conta bradesco = new Conta();
    @FXML
    private void addPessoa()
    {
        
        joao.setNome(nome.getText());
        joao.setEndereco(endereco.getText());
        joao.setCidade(cidade.getText());
        joao.setBairro(bairro.getText());
        joao.setEstado(estado.getText());
        joao.setFone(Integer.parseInt(fone.getText()));
        joao.setEmail(email.getText());
        joao.setCpf(cpf.getText());
        joao.setRg(rg.getText());
    }
    @FXML
    private void addConta()
    {
        bradesco.setTitular(joao);
        bradesco.setNumero(Integer.parseInt(numero.getText()));
        bradesco.setDigito(Integer.parseInt(digito.getText()));
        bradesco.setSaldo(Float.parseFloat(saldo.getText()));
        
    }
    @FXML
    private void creditar()
    {
        bradesco.credita(10);
        respcre.setText("10 reais creditado");
    }
    @FXML
    private void debitar()
    {
        bradesco.debita(80);
        if(bradesco.debita(80))
        {
            respdeb.setText("Valor debitado com sucesso");
        }
        else
        {
            respdeb.setText("Saldo insuficiente!");
        }
    }
}
